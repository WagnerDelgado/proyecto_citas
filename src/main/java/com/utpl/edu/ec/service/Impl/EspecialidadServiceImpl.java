package com.utpl.edu.ec.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utpl.edu.ec.model.Especialidad;
import com.utpl.edu.ec.repo.IEspecialidadRepo;
import com.utpl.edu.ec.repo.IGenericRepo;
import com.utpl.edu.ec.repo.IMedicoRepo;
import com.utpl.edu.ec.services.IEspecialidadService;

@Service
public class EspecialidadServiceImpl extends CRUDImpl<Especialidad, Integer> implements IEspecialidadService{

	@Autowired
	private IEspecialidadRepo repo;

	@Override
	protected IGenericRepo<Especialidad, Integer> getRepo() {
		return repo;
	}

}
