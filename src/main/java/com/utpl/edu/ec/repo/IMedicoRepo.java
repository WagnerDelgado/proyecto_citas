package com.utpl.edu.ec.repo;

import com.utpl.edu.ec.model.Medico;

public interface IMedicoRepo extends IGenericRepo<Medico, Integer>{

}
