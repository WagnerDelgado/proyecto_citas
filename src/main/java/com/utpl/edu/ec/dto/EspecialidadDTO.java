package com.utpl.edu.ec.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class EspecialidadDTO {
	
	private Integer IdEspecialidad;
	@NotEmpty
	private String nombre;
	private String descripcion;
	
	public Integer getIdEspecialidad() {
		return IdEspecialidad;
	}
	public void setIdEspecialidad(Integer idEspecialidad) {
		IdEspecialidad = idEspecialidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
